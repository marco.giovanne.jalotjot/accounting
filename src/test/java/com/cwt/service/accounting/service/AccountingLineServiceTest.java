package com.cwt.service.accounting.service;

import com.cwt.service.accounting.constant.Status;
import com.cwt.service.accounting.controller.CommonResponse;
import com.cwt.service.accounting.controller.request.AccountingLineRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.util.Map;


public class AccountingLineServiceTest {

    AccountingLineService service;

    AccountingLineRequest request;

    @BeforeEach
    public void init() {
        service = new AccountingLineServiceImpl();
        request = new AccountingLineRequest();
        request.setGstAmount(new BigDecimal("10"));
        request.setQstAmount(new BigDecimal("10"));
        request.setTaxAmount(new BigDecimal("201.38"));
        request.setNumberOfConjunctedDocuments("15-FEE-EXP$08");
        service.abc(request);
    }

    @Test
    public void shouldReturnActive() {
        CommonResponse<Map<String, Object>> result = service.getAbcData("accountingLineStatus");

        Assertions.assertEquals(result.getCode(), HttpStatus.OK.value());
        Assertions.assertEquals(result.getData().get("accountingLineStatus"), Status.ACTIVE);
    }

    @Test
    public void shouldReturnTotalTaxAmount() {
        CommonResponse<Map<String, Object>> result = service.getAbcData("totalTaxAmount");

        Assertions.assertEquals(result.getCode(), HttpStatus.OK.value());
        Assertions.assertEquals(result.getData().get("totalTaxAmount"), 221.38);

    }
}
