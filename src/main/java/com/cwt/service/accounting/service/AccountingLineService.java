package com.cwt.service.accounting.service;

import com.cwt.service.accounting.controller.CommonResponse;
import com.cwt.service.accounting.controller.request.AccountingLineRequest;

import java.util.Map;

public interface AccountingLineService {
    CommonResponse<AccountingLineRequest> abc(final AccountingLineRequest request);
    CommonResponse<Map<String, Object>> getAbcData(final String property);
}
