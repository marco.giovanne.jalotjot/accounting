package com.cwt.service.accounting.service;

import com.cwt.service.accounting.until.CommonUtil;
import com.cwt.service.accounting.constant.CommonResponseMessage;
import com.cwt.service.accounting.constant.Status;
import com.cwt.service.accounting.controller.CommonResponse;
import com.cwt.service.accounting.controller.request.AccountingLineRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.cwt.service.accounting.constant.AbcProperty.*;

/**
 * @author Marco Giovanne
 */
@Service
public class AccountingLineServiceImpl implements AccountingLineService {

    /**
     * instance storage of data
     */
    private Map<String, Object> abc;

    @Override
    public CommonResponse<AccountingLineRequest> abc(final AccountingLineRequest request) throws RuntimeException{
        manageRequest(request);
        return new CommonResponse<>(HttpStatus.OK.value(), CommonResponseMessage.OK, request);
    }

    @Override
    public CommonResponse<Map<String, Object>> getAbcData(String property) throws RuntimeException {
        Map<String, Object> data = new HashMap<>();

        if(ObjectUtils.isEmpty(abc)) {
            return new CommonResponse<>(HttpStatus.BAD_REQUEST.value(), CommonResponseMessage.NO_ABC, null);
        }

        Object value = abc.get(property);

        if(ObjectUtils.isEmpty(value)) {
            return new CommonResponse<>(HttpStatus.NOT_FOUND.value(), CommonResponseMessage.NOT_FOUND, null);
        }

        data.put(property, value);
        return new CommonResponse<>(HttpStatus.OK.value(), CommonResponseMessage.OK, data);
    }

    private void manageRequest(final AccountingLineRequest request) {
        final ObjectMapper mapper = new ObjectMapper();
        abc = mapper.convertValue(request, HashMap.class);
        BigDecimal totalTaxSurcharge = request.getGstAmount().add(request.getQstAmount());
        abc.put(ACCOUNTING_LINE_STATUS, Status.ACTIVE);
        abc.put(TRAVELER_REF_ID_LIST, 1);
        abc.put(TOTAL_TAX_SURCHARGE, totalTaxSurcharge);
        abc.put(TOTAL_TAX_AMOUNT, request.getTaxAmount().add(totalTaxSurcharge).doubleValue());
        abc.put(NUMBER_OF_CONJUCTED_DOCUMENTS, CommonUtil.builder().build().parseNumberOfConjunctedDocuments((String) abc.get(NUMBER_OF_CONJUCTED_DOCUMENTS)));
    }


}
