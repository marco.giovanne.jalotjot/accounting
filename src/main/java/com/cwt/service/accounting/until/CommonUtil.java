package com.cwt.service.accounting.until;

import lombok.Builder;

@Builder
public class CommonUtil {

    public String parseNumberOfConjunctedDocuments(String data) {
        String[] arr = data.split("-");
        return arr[0] + '-' + arr[1];
    }
}
