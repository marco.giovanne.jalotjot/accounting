package com.cwt.service.accounting.constant;

public interface AbcProperty {
    String ACCOUNTING_LINE_STATUS = "accountingLineStatus";
    String TRAVELER_REF_ID_LIST = "travelerRefIDList";
    String TOTAL_TAX_SURCHARGE = "totalTaxSurcharge";
    String TOTAL_TAX_AMOUNT = "totalTaxAmount";
    String NUMBER_OF_CONJUCTED_DOCUMENTS = "numberOfConjunctedDocuments";

}
