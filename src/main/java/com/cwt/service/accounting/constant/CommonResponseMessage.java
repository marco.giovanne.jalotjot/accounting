package com.cwt.service.accounting.constant;

public interface CommonResponseMessage {
    String OK = "Ok";
    String NOT_FOUND = "No available data";
    String NO_ABC = "ABC not available, please upload XYZ";
    String INTERNAL_ERROR = "Oh noo! we encounter error.";
}
