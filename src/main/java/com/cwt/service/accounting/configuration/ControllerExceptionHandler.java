package com.cwt.service.accounting.configuration;

import com.cwt.service.accounting.controller.CommonResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static com.cwt.service.accounting.constant.CommonResponseMessage.INTERNAL_ERROR;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ControllerExceptionHandler  {

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> internalServerError(Exception ex) {
        return new ResponseEntity<>(new CommonResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), INTERNAL_ERROR, null), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
