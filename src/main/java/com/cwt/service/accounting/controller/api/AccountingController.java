package com.cwt.service.accounting.controller.api;


import com.cwt.service.accounting.controller.CommonResponse;
import com.cwt.service.accounting.controller.request.AccountingLineRequest;
import com.cwt.service.accounting.service.AccountingLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping(path = "/api/v1/accountingLine")
public class AccountingController {

    private final AccountingLineService accountingLineService;

    @Autowired
    public AccountingController(final AccountingLineService accountingLineService) {
        this.accountingLineService = accountingLineService;
    }

    @GetMapping("/ping")
    public ResponseEntity<Object> ping() {
        Map<String, String> map = new HashMap<>();
        map.put("ping", "pong");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping(value = "/ABC")
    public ResponseEntity<CommonResponse<AccountingLineRequest>> abc(@RequestBody AccountingLineRequest request) {
        final CommonResponse<AccountingLineRequest> result = accountingLineService.abc(request);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping("/{property}")
    public ResponseEntity<CommonResponse<Map<String, Object>>> abcData(@PathVariable("property") String prop) {
        final CommonResponse<Map<String, Object>> result = accountingLineService.getAbcData(prop);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }



}
