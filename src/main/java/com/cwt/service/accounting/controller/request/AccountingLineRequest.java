package com.cwt.service.accounting.controller.request;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "AccountingLine")
public class AccountingLineRequest implements Serializable {

    @XmlAttribute
    private Integer id;

    @XmlAttribute
    private Integer index;

    @XmlAttribute
    private String op;

    @XmlAttribute
    private String elementId;

    @JacksonXmlProperty(localName = "TypeIndicator")
    private String typeIndicator;

    @JacksonXmlProperty(localName = "FareApplication")
    private String fareApplication;

    @JacksonXmlProperty(localName = "FormOfPaymentCode")
    private String formOfPaymentCode;

    @JacksonXmlProperty(localName = "LinkCode")
    private String linkCode;

    @JacksonXmlProperty(localName = "AccountingVendorCode")
    private String accountingVendorCode;

    @JacksonXmlProperty(localName = "ChargeCategoryCoded")
    private String chargeCategoryCoded;

    @JacksonXmlProperty(localName = "AirlineDesignator")
    private String airlineDesignator;

    @JacksonXmlProperty(localName = "DocumentNumber")
    private String documentNumber;

    @JacksonXmlProperty(localName = "CommissionPercentage")
    private Double commissionPercentage;

    @JacksonXmlProperty(localName = "CommissionAmount")
    private BigDecimal commissionAmount;

    @JacksonXmlProperty(localName = "BaseFare")
    private BigDecimal baseFare;

    @JacksonXmlProperty(localName = "TaxPercentage")
    private Double taxPercentage;

    @JacksonXmlProperty(localName = "TaxAmount")
    private BigDecimal taxAmount;

    @JacksonXmlProperty(localName = "TaxSurchargeCode2")
    private String taxSurchargeCode2;

    @JacksonXmlProperty(localName = "GSTCode")
    private String gstCode;

    @JacksonXmlProperty(localName = "GSTAmount")
    private BigDecimal gstAmount;

    @JacksonXmlProperty(localName = "GSTPercent")
    private Double gstPercent;

    @JacksonXmlProperty(localName = "QSTCode")
    private String qstCode;

    @JacksonXmlProperty(localName = "QSTAmount")
    private BigDecimal qstAmount;

    @JacksonXmlProperty(localName = "QSTPercent")
    private Double qstPercent;

    @JacksonXmlProperty(localName = "CreditCardNumber")
    private String creditCardNumber;

    @JacksonXmlProperty(localName = "CreditCardCode")
    private String creditCardCode;

    @JacksonXmlProperty(localName = "PassengerName")
    private String passengerName;

    @JacksonXmlProperty(localName = "NumberOfConjunctedDocuments")
    private String numberOfConjunctedDocuments;

    @JacksonXmlProperty(localName = "NumberOfCoupons")
    private Integer numberOfCoupons;

    @JacksonXmlProperty(localName = "OriginalTicketNumber")
    private String OriginalTicketNumber;

    @JacksonXmlProperty(localName = "OriginalDateOfIssue")
    private String originalDateOfIssue;

    @JacksonXmlProperty(localName = "OriginalPlaceOfIssue")
    private String originalPlaceOfIssue;

    @JacksonXmlProperty(localName = "FullPartialExchangeIndicator")
    private String fullPartialExchangeIndicator;

    @JacksonXmlProperty(localName = "OriginalInvoice")
    private String originalInvoice;

    @JacksonXmlProperty(localName = "TarriffBasis")
    private String tarriffBasis;

    @JacksonXmlProperty(localName = "FreeFormText")
    private String freeFormText;

    @JacksonXmlProperty(localName = "CurrencyCode")
    private String currencyCode;

    @JacksonXmlProperty(localName = "SegmentType")
    private String segmentType;

    @JacksonXmlProperty(localName = "SegmentNumber")
    private Integer segmentNumber;

}
